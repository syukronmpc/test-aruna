package test.app.aruna.data.api

import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.common.truth.Truth.assertThat
import test.app.aruna.data.util.Response

class PostsApiServiceTest {
    private lateinit var service: PostsApiService
    private lateinit var server: MockWebServer

    @Before
    fun setUp() {
        server = MockWebServer()
        service = Retrofit.Builder().baseUrl(server.url(path = ""))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(PostsApiService::class.java)
    }

    @After
    fun tearDown() {
        server.shutdown()
    }

    private fun enqueueMockResponse(
        fileName: String
    ){
        val inputStream = javaClass.classLoader!!.getResourceAsStream(fileName)
        val source = inputStream.source().buffer()
        val mockResponse = MockResponse()
        mockResponse.setBody(source.readString(Charsets.UTF_8))
        server.enqueue(mockResponse)
    }

    @Test
    fun getPosts_sentRequest_receivedExpected() {
        runBlocking {
            enqueueMockResponse("postsresponse.json")
            val responseBody = Response.Success(service.getPosts())
            val request = server.takeRequest()

            assertThat(responseBody).isNotNull()
            assertThat(request.path).isEqualTo("/posts")
        }
    }

    @Test
    fun getPosts_receivedResponse_correctContent() {
        runBlocking {
            enqueueMockResponse("postsresponse.json")
            val responseBody = Response.Success(service.getPosts())
            val post = responseBody.data!![0]
            assertThat(post.userId).isEqualTo(1)
            assertThat(post.id).isEqualTo(1)
            assertThat(post.title).isEqualTo("sunt aut facere repellat provident occaecati excepturi optio reprehenderit")
            assertThat(post.body).isEqualTo("quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto")
        }
    }
}