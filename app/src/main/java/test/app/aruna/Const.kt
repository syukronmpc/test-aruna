package test.app.aruna

object Const {
    // Screens
    const val HOME_SCREEN = "home"
    const val DETAIL_SCREEN = "detail"

    const val API_BASE_URL = "https://jsonplaceholder.typicode.com"
}