package test.app.aruna

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import dagger.hilt.android.AndroidEntryPoint
import test.app.aruna.fragment.DetailFragment
import test.app.aruna.fragment.HomeFragment
import test.app.aruna.ui.theme.ArunaTestTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ArunaComposeAppScreen()
        }
    }

    @Composable
    fun ArunaComposeAppScreen() {
        val navController = rememberNavController()
        NavHost(
            navController = navController,
            startDestination = Route.Home.route,
        ) {
            composable(Route.Home.route) {
                HomeFragment(
                    onClickToDetailScreen = { postId ->
                        navController.navigate(
                            Route.Detail.createRoute(postId)
                        )
                    }
                )
            }
            Log.e("TAG", "ArunaComposeAppScreen: " + Route.Detail.route)
            
            composable(
                Route.Detail.route,
                arguments = listOf(
                    navArgument("postId"){
                        type = NavType.IntType
                    }
                )
            ) { backStackEntry ->
                val postId = backStackEntry.arguments?.getInt("postId")
                requireNotNull(postId) { "ID parameter wasn't found. Please make sure it's set!" }
                DetailFragment(
                    postId,
                    onBackPress = {
                        navController.popBackStack()
                    }
                )
            }
        }
    }

    @Preview(showBackground = true)
    @Composable
    fun ArunaComposeAppPreview() {
        ArunaTestTheme() {
            ArunaComposeAppScreen()
        }
    }
}


