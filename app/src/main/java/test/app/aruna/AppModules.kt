package test.app.aruna

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import test.app.aruna.data.api.PostsApiService
import test.app.aruna.data.repository.PostsRepositoryImpl
import test.app.aruna.domain.repository.PostsRepository
import javax.inject.Named

@Module
@InstallIn(SingletonComponent::class)
class AppModules {

    @Provides
    @Named("API_BASE_URL")
    fun provideAPIBaseUrl(): String = "https://jsonplaceholder.typicode.com"


    @Provides
    fun provideRetrofit(
        @Named("API_BASE_URL") baseUrl: String,
    ): Retrofit {
        val client = OkHttpClient
            .Builder()
            .addInterceptor(
                HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY)
            )
            .build()
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    @Provides
    fun providePostsApiService(
        retrofit: Retrofit
    ): PostsApiService = retrofit.create(PostsApiService::class.java)

    @Provides
    fun providePostsRepository(
        postsApiService: PostsApiService,
    ): PostsRepository = PostsRepositoryImpl(
        postsApiService = postsApiService
    )
}