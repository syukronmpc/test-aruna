package test.app.aruna

sealed class Route(val route: String) {
    object Home: Route(Const.HOME_SCREEN)
    object Detail: Route("${Const.DETAIL_SCREEN}/{postId}") {
       fun createRoute(postId: Int) = "${Const.DETAIL_SCREEN}/$postId"
    }
}
