package test.app.aruna.compose

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import test.app.aruna.ui.theme.Purple700
import test.app.aruna.ui.theme.White

@Composable
fun TopAppBarWithBack(title: String, onBackPress: () -> Unit) {
    TopAppBar(
        title = {
            Text(text = title)
        },
        navigationIcon = {
            IconButton(onClick = onBackPress) {
                Icon(Icons.Filled.ArrowBack, "backIcon")

            }
        },
        backgroundColor = Purple700,
        contentColor = White,
        elevation = 8.dp
    )
}

@Composable
fun TopAppBarWithoutBack(title: String) {
    TopAppBar(
        title = {
            Text(text = title)
        },
        backgroundColor = Purple700,
        contentColor = White,
        elevation = 8.dp
    )
}

@Composable
fun Container(
    title: String,
    canBack: Boolean = false,
    onBackPress: () -> Unit = {},
    content: @Composable() () -> Unit
) {
    Scaffold(
        topBar = {if (canBack) TopAppBarWithBack(title, onBackPress) else TopAppBarWithoutBack(title)}, content = {
            Column(
                modifier = Modifier
                    .background(White)
                    .fillMaxSize()
                    .padding(it),
            ) {
                content()
            }
        }
    )
}