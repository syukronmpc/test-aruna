package test.app.aruna.domain.repository

import kotlinx.coroutines.flow.Flow
import test.app.aruna.data.model.PostsResponse
import test.app.aruna.data.model.PostsResponseItem
import test.app.aruna.data.util.Response

interface PostsRepository {
    fun getPosts(): Flow<Response<PostsResponse>>
    fun getDetailPost(id: Int): Flow<Response<PostsResponseItem>>
}