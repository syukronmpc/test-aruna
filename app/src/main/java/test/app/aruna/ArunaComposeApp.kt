package test.app.aruna

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ArunaComposeApp: Application()