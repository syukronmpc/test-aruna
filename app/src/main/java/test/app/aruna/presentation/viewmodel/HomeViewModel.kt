package test.app.aruna.presentation.viewmodel

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import test.app.aruna.data.model.PostsResponse
import test.app.aruna.data.util.Response
import test.app.aruna.domain.repository.PostsRepository
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val postsRepository: PostsRepository,
): ViewModel() {
    private val _postsState = mutableStateOf<Response<PostsResponse>>(Response.Success(null))
    val postsState: State<Response<PostsResponse>> = _postsState

    fun getPosts()  {
        viewModelScope.launch {
            postsRepository.getPosts().collect() { response ->
                _postsState.value = response
            }
        }
    }
}