package test.app.aruna.presentation.viewmodel

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import test.app.aruna.data.model.PostsResponse
import test.app.aruna.data.model.PostsResponseItem
import test.app.aruna.data.util.Response
import test.app.aruna.domain.repository.PostsRepository
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val postsRepository: PostsRepository
): ViewModel() {
    private val _postsDetail = mutableStateOf<Response<PostsResponseItem>>(Response.Success(null))
    val postsStateDetail: State<Response<PostsResponseItem>> = _postsDetail

    fun getPostDetail(id: Int)  {
        viewModelScope.launch {
            postsRepository.getDetailPost(id).collect() { response ->
                _postsDetail.value = response
            }
        }
    }
}