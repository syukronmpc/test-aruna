package test.app.aruna.presentation.view

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import test.app.aruna.data.model.PostsResponseItem
import test.app.aruna.ui.theme.Black

@Composable
fun DetailScreen(
    post: PostsResponseItem? = null,
) {
    if (post == null) return
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState())
            .padding(8.dp)
    ){
        Text(
            post.title,
            color = Black,
            fontWeight = FontWeight.Bold,
            fontSize = 18.sp,
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(post.body, fontSize = 14.sp, color = Black)
    }
}