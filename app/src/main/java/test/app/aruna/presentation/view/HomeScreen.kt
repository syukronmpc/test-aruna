package test.app.aruna.presentation.view

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import test.app.aruna.compose.PostItem
import test.app.aruna.data.model.PostsResponse
import test.app.aruna.data.model.PostsResponseItem
import test.app.aruna.ui.theme.Black
import test.app.aruna.ui.theme.Purple700
import test.app.aruna.ui.theme.Shapes

@Composable
fun HomeScreen(
    modifier: Modifier = Modifier,
    posts: PostsResponse? = null,
    onClickToDetailScreen: (Int) -> Unit = {},
) {
    var searchText by remember {
        mutableStateOf("")
    }
    if (posts == null) return
    val postsFilter = if (searchText != "") posts.filter {
            item -> item.title.contains(searchText)
    } as ArrayList<PostsResponseItem> else posts

    Column(
        modifier = Modifier.fillMaxSize(),
    ) {
        OutlinedTextField(
            value = searchText,
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Purple700,
                unfocusedBorderColor = Black,
                textColor = Black,
            ),
            onValueChange = {searchText = it},
            shape = Shapes.medium,
            placeholder = {Text("Search Post Title", color = Color.Gray)},
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp),
        )
        Spacer(modifier = Modifier.height(8.dp))
        LazyColumn(
            modifier = modifier.fillMaxSize(),
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            items(postsFilter.size) {index ->
                PostItem(id = postsFilter[index].id, title = postsFilter[index].title, onClick = onClickToDetailScreen)
            }
        }
    }


}