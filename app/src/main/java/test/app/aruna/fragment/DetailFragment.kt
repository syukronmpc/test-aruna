package test.app.aruna.fragment

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import test.app.aruna.compose.Container
import test.app.aruna.compose.ErrorButton
import test.app.aruna.compose.Loading
import test.app.aruna.data.util.Response
import test.app.aruna.presentation.view.DetailScreen
import test.app.aruna.presentation.viewmodel.DetailViewModel
import test.app.aruna.ui.theme.ArunaTestTheme

@Composable
fun DetailFragment(
    postId: Int = -1,
    detailViewModel: DetailViewModel = hiltViewModel(),
    title: String = "Detail Post",
    onBackPress: () -> Unit = {}
) {
    fun launch() {
        detailViewModel.getPostDetail(postId);
    }
    launch()
    ArunaTestTheme {
        when(val detailResponse = detailViewModel.postsStateDetail.value){
            is Response.Loading -> {
                Container(title, canBack = true, onBackPress) {
                    Loading(
                        modifier = Modifier.fillMaxSize()
                    )
                }
            }
            is Response.Success -> {
                Container(title, canBack = true, onBackPress) {
                    DetailScreen(
                        post = detailResponse.data
                    )
                }
            }
            is Response.Failure -> {
                Container(title, canBack = true, onBackPress) {
                    ErrorButton(
                        modifier = Modifier.fillMaxWidth(),
                        text = "Error occurred. Please try again",
                        onClick = {
                            launch()
                        }
                    )
                }
            }
        }
    }
}