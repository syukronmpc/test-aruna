package test.app.aruna.fragment

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import test.app.aruna.compose.Container
import test.app.aruna.compose.ErrorButton
import test.app.aruna.compose.Loading
import test.app.aruna.data.model.PostsResponse
import test.app.aruna.data.model.PostsResponseItem
import test.app.aruna.data.util.Response
import test.app.aruna.presentation.view.HomeScreen
import test.app.aruna.presentation.viewmodel.HomeViewModel
import test.app.aruna.ui.theme.ArunaTestTheme
import test.app.aruna.ui.theme.White

@Composable
fun HomeFragment(
    modifier: Modifier = Modifier,
    homeViewModel: HomeViewModel = hiltViewModel(),
    onClickToDetailScreen: (Int) -> Unit = {},
    title: String = "Posts",
) {
    fun launch() {
        homeViewModel.getPosts()
    }
    launch()
    ArunaTestTheme {
        when(val postsResponse = homeViewModel.postsState.value) {
            is Response.Loading -> {
                Container(title) {
                    Loading(
                        modifier = Modifier.fillMaxSize()
                    )
                }
            }
            is Response.Success -> {
                Container(title) {
                    HomeScreen(
                        posts = postsResponse.data,
                        onClickToDetailScreen = onClickToDetailScreen
                    )
                }
            }
            is Response.Failure -> {
                Container(title) {
                    ErrorButton(
                        modifier = Modifier.fillMaxWidth(),
                        text = "Error occured, Please Try Again",
                        onClick = {
                            launch()
                        }
                    )
                }
            }
        }
    }
}