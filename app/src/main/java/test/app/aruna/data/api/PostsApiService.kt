package test.app.aruna.data.api

import retrofit2.http.GET
import retrofit2.http.Path
import test.app.aruna.data.model.PostsResponse
import test.app.aruna.data.model.PostsResponseItem

interface PostsApiService {
    @GET("/posts")
    suspend fun getPosts(): PostsResponse

    @GET("/posts/{id}")
    suspend fun getDetailPost(
        @Path("id") id: Int
    ): PostsResponseItem
}