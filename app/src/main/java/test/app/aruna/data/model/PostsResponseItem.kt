package test.app.aruna.data.model

data class PostsResponseItem(
    val body: String,
    val id: Int,
    val title: String,
    val userId: Int
)