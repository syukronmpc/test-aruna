package test.app.aruna.data.repository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import test.app.aruna.data.api.PostsApiService
import test.app.aruna.data.model.PostsResponse
import test.app.aruna.data.model.PostsResponseItem
import test.app.aruna.data.util.Response
import test.app.aruna.domain.repository.PostsRepository
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PostsRepositoryImpl @Inject constructor(
    private val postsApiService: PostsApiService,
): PostsRepository {
    override fun getPosts(): Flow<Response<PostsResponse>> = flow{
        try {
            emit(Response.Loading)
            val responseApi = postsApiService.getPosts()
            emit(Response.Success(responseApi))
        } catch (e: Exception) {
            emit(Response.Failure(e))
        }
    }.flowOn(Dispatchers.IO)

    override fun getDetailPost(id: Int): Flow<Response<PostsResponseItem>> = flow{
        try {
            emit(Response.Loading)
            val responseApi = postsApiService.getDetailPost(id)
            emit(Response.Success(responseApi))
        } catch (e: Exception) {
            emit(Response.Failure(e))
        }
    }.flowOn(Dispatchers.IO)
}